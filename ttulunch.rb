#!/usr/bin/env ruby
require 'net/http'
require 'rubygems'
require 'nokogiri'
require 'date'
require 'money'
require 'slim'
require 'pdf-reader'
require 'pp'

I18n.config.available_locales = :en

module EstonianMonths
  MONTH_LIST = %w(jaanuar veebruar märts aprill mai juuni juuli august september oktoober november detsember)
  def self.month_name_to_number(month)
    MONTH_LIST.find_index(month) + 1
  end
end

class RahvaToitFb
  POSTS_URI = URI.parse("https://www.facebook.com/rahvatoitttu/posts/")
  
  private
  
  def get_data
    response = Net::HTTP.get(POSTS_URI)
    page = Nokogiri::HTML(response)
    page.css(".text_exposed_hide").remove
    page.css("#content_container .userContentWrapper").map { |userStory|
      item = {}
      item[:time] = Time.at(userStory.at_css("abbr[data-utime]")["data-utime"].to_i)
      item[:text] = []
      userStory.at_css(".userContent").traverse { |node|
        if node.text?
          item[:text] << node.text.strip
        end
      }
      item
    }
  end
  
  def parse_type(line)
    if line.match(/soc[ \-]?maja/)
      :soc
    elsif line.include?("raamatukogu")
      :lib
    else
      nil
    end
  end
  
  def parse_item(line)
    match = line.match(/^(?:(.+?)(?:\s*\/\s*(.+))?:\s?)?(.+?)(?:\s*\/\s*(.+?))?\s+([0-9]+)(?:[.,]([0-9]+))?\s*€?$/)
    if match.nil?
      nil
    else
      {
        estonian: match[1].nil? ? match[3] : (match[1] + ": " + match[3]),
        english: match[2].nil? ? match[4] : (match[2] + ": " + match[4]),
        price: Money.new(match[5].to_i * 100 + match[6].to_i, "EUR")
      }
    end
  end
  
  def parse_items(lines)
    lines
      .slice_after { |line| !line.match(/\/\s*$/) }
      .map(&:join)
      .map { |line| parse_item(line) }
      .compact
  end
  
  def parse(data)
    data.map { |post|
      parsed = {
        date: post[:time].to_date,
        cafeteria: parse_type(post[:text][0].downcase),
        source: POSTS_URI.to_s,
        items: parse_items(post[:text][1..-1])
      }
      parsed
    }.reject { |post| post[:cafeteria].nil? }
  end
  
  public
   
  def fetch
    parse(get_data)
  end
end

class DailyPDF
  def initialize(cafeteria, uri)
    @cafeteria = cafeteria
    @uri = uri
  end

  private
  
  DESCRIPTION_PRICE_REGEXP = /^(.+?)\s+([0-9]+)\.([0-9]{2})$/
  DATE_REGEXP = /(\d+)\.\s+(\w+)$/
  
  def parse_item(lines)
    match = lines[0].match(DESCRIPTION_PRICE_REGEXP)
    {
      estonian: match[1],
      english: lines[1],
      price: Money.new(match[2].to_i * 100 + match[3].to_i, "EUR")
    }
  end
  
  def parse_day_items(lines)
    # * group lines by item (always starts with price line)
    # * filter out noise
    # * parse each item
    lines
      .slice_before { |line| !line.match(DESCRIPTION_PRICE_REGEXP).nil? }
      .reject { |item_lines| item_lines.count != 2 }
      .map { |item_lines| parse_item(item_lines) }
  end
  
  def parse_day_date(line)
    match = line.match(DATE_REGEXP)
    Date.new(Date.today.year, EstonianMonths.month_name_to_number(match[2]), match[1].to_i)
  end
  
  def parse_day(lines)
    {
      date: parse_day_date(lines[0]),
      cafeteria: @cafeteria,
      source: @uri.to_s,
      items: parse_day_items(lines[1..-1])
    }
  end
  
  def parse(lines)
    lines
      .reject(&:empty?)
      .slice_before { |line| # group by date
        !line.match(DATE_REGEXP).nil?
      }
      .drop(1) # skip header
      .map { |day_lines| parse_day(day_lines) }
  end
  
  public
  
  def fetch
    pdf_data = Net::HTTP.get(@uri)
    pdf = PDF::Reader.new(StringIO.new(pdf_data))
    text = pdf.pages.map(&:text).join("\n").split("\n")
    parse(text)
  end
end

class MenuProcessor
  DAILY_URLS = {
    u01: "http://www.daily.lv/download/?f=dn_ehitajate_tee_5_tallinn_(ttu_peahoone).pdf",
#    u04: "http://www.daily.lv/download/?f=dn_ttu_4_korpus.pdf",
    itc: "http://www.daily.lv/download/?f=dn_ttu_it_maja.pdf",
#    sci: "http://www.daily.lv/download/?f=dn_ttu_keemiainstituut.pdf"
  }

  def fetch_all_menus
    menus = []
    DAILY_URLS.each do |cafeteria, url|
      menus += DailyPDF.new(cafeteria, URI.parse(url)).fetch
    end
    menus += RahvaToitFb.new.fetch
  end
  
  def cafeteria_name(cafeteria)
    cafeteria.to_s.upcase
  end
  
  def menus_today
    fetch_all_menus
      .select { |menu| menu[:date] == Date.today }
      .map { |menu|
        [menu[:cafeteria], {
          name: cafeteria_name(menu[:cafeteria]),
          source: menu[:source],
          items: menu[:items]
        }]
      }
  end
end

class SlimEnvironment
  attr_accessor :cafeterias
  attr_accessor :date
end

processor = MenuProcessor.new

env = SlimEnvironment.new
env.cafeterias = processor.menus_today
env.date = Time.now

print Slim::Template.new('ttulunch.slim', pretty: true).render(env)
